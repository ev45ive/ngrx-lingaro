import { Action } from "@ngrx/store";

export enum CounterActionTypes {
  ResetCounters = "[Counter] Reset Counters",
  Increment = "[Counter] Increment",
  Decrement = "[Counter] Decrement"
}

export class ResetCounters implements Action {
  readonly type = CounterActionTypes.ResetCounters;
}

export class Increment implements Action {
  readonly type = CounterActionTypes.Increment;
  constructor(readonly payload = 1) {}
}

export class Decrement implements Action {
  readonly type = CounterActionTypes.Decrement;
  constructor(readonly payload = 1) {}
}

export type CounterActions = ResetCounters | Increment | Decrement;
