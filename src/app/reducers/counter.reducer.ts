import { Action } from "@ngrx/store";
import { CounterActions, CounterActionTypes } from "./counter.actions";

export const counterFeatureKey = "counter";

export interface State {
  value: number;
}

export const initialState: State = {
  value: 0
};

export function reducer(state = initialState, action: CounterActions): State {
  switch (action.type) {
 
    case CounterActionTypes.ResetCounters:
      return {
        value: 0
      };

    case CounterActionTypes.Increment:
      return {
        value: state.value + action.payload
      };

    case CounterActionTypes.Decrement:
      return {
        value: state.value - action.payload
      };

    default:
      return state;
  }
}
