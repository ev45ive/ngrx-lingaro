import {
  ActionReducer,
  ActionReducerMap,
  createFeatureSelector,
  createSelector,
  MetaReducer,
  combineReducers
} from "@ngrx/store";
import { environment } from "../../environments/environment";
import * as fromCounter from "./counter.reducer";
import * as fromRouter from "@ngrx/router-store";

export interface AppState {
  // [`dynamic property placki`+123]:123,
  [fromCounter.counterFeatureKey]: fromCounter.State;
  router: fromRouter.RouterReducerState;
}

export const reducers: ActionReducerMap<AppState> = {
  [fromCounter.counterFeatureKey]: fromCounter.reducer,
  router: fromRouter.routerReducer

  // subSubKey: combineReducers({
  //   placki:plackiReducer
  // })
};

export const metaReducers: MetaReducer<AppState>[] = !environment.production
  ? [
      // LoggerMetaReducer
    ]
  : [];
