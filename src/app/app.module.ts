import { BrowserModule } from "@angular/platform-browser";
import { NgModule } from "@angular/core";

import { AppRoutingModule } from "./app-routing.module";
import { AppComponent } from "./app.component";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { TodosModule } from "./todos/todos.module";
import { NavLayoutComponent } from "./components/nav-layout/nav-layout.component";
import { LayoutModule } from "@angular/cdk/layout";
import { MatToolbarModule } from "@angular/material/toolbar";
import { MatButtonModule } from "@angular/material/button";
import { MatSidenavModule } from "@angular/material/sidenav";
import { MatIconModule } from "@angular/material/icon";
import { MatListModule } from "@angular/material/list";
import { UsersModule } from "./users/users.module";
import { StoreModule } from "@ngrx/store";
import { reducers, metaReducers } from "./reducers";
import { StoreDevtoolsModule } from "@ngrx/store-devtools";
import { environment } from "../environments/environment";
import { CounterComponent } from "./pages/counter/counter.component";
import { TasksModule } from "./tasks/tasks.module";
import { EffectsModule } from "@ngrx/effects";

import { StoreRouterConnectingModule, routerReducer } from "@ngrx/router-store";
import { MessengerModule } from "./messenger/messenger.module";
import { EntityDataModule, DefaultDataServiceConfig } from "@ngrx/data";

@NgModule({
  declarations: [AppComponent, NavLayoutComponent, CounterComponent],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    TodosModule,
    LayoutModule,
    MatToolbarModule,
    MatButtonModule,
    MatSidenavModule,
    MatIconModule,
    MatListModule,
    UsersModule,
    StoreModule.forRoot(reducers, {
      metaReducers,
      runtimeChecks: {
        strictStateImmutability: true,
        strictActionImmutability: true
      }
    }),
    EffectsModule.forRoot([]),

    // Connects RouterModule with StoreModule
    StoreRouterConnectingModule.forRoot({}),

    EntityDataModule.forRoot({
      entityMetadata: {
        Post: {},
        Comment: {}
      }
    }),

    !environment.production ? StoreDevtoolsModule.instrument() : [],
    TasksModule,
    MessengerModule
  ],
  providers: [
    {
      provide: DefaultDataServiceConfig,
      useValue: {
        root: "http://localhost:3000/"
      }
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}
