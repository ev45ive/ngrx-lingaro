import { createFeatureSelector, createSelector } from "@ngrx/store";
import { userFeatureKey, State } from "./reducers/user.reducer";
import { User } from "../models/user";
import { selectRouteParam } from "../reducers/router.reducer";

export const usersFeature = createFeatureSelector<State>(userFeatureKey);

export const selectUsersList = createSelector(
  usersFeature,
  state => state.users
);

export const selectUsersError = createSelector(
  usersFeature,
  state => state.error
);

export const selectUsersLoading = createSelector(
  usersFeature,
  state => state.isLoading
);

export const selectUserDetails = createSelector(
  usersFeature,
  (state, { user_id }) => state.users.find(u => u.id == user_id)
);

interface AppState {
  users: State;
}

export const selectedUserDetails = createSelector(
  usersFeature,
  selectRouteParam("user_id"),
  (state, user_id) => state.users.find(u => u.id == parseInt(user_id))
);
