import { Injectable } from "@angular/core";
import { Actions, createEffect, ofType } from "@ngrx/effects";

import {
  concatMap,
  map,
  mergeAll,
  concatAll,
  switchAll,
  mergeMap,
  exhaustMap,
  catchError,
  mapTo,
  filter,
  tap
} from "rxjs/operators";
import { EMPTY, of, from, timer } from "rxjs";
import { ROUTER_NAVIGATED, RouterNavigatedPayload } from "@ngrx/router-store";
import * as UserActions from "../actions/user.actions";
import { HttpClient } from "@angular/common/http";
import { User } from "src/app/models/user";
import { Store } from "@ngrx/store";
import { AppState } from "src/app/reducers";

@Injectable()
export class UserEffects {
  constructor(
    private store: Store<AppState>,
    private http: HttpClient,
    private actions$: Actions
  ) {}

  nagivated$ = createEffect(() =>
    this.actions$.pipe(
      ofType(ROUTER_NAVIGATED),
      filter(
        (action: { payload: RouterNavigatedPayload }) =>
          action.payload.routerState.url == "/users"
      ),
      mapTo(UserActions.fetchUsers())
    )
  );

  saveUser$ = createEffect(() =>
    this.actions$.pipe(
      ofType(UserActions.saveUser),
      exhaustMap(({ user }) =>
        from([
          of(UserActions.loadingUsers()),
          this.saveUserAPIRequest(user).pipe(
            map(user => UserActions.updateUser({ user })),
            catchError(error => of(UserActions.errorLoadingUsers({ error })))
          )
        ])
      ),
      mergeAll()
    )
  );

  loadUsers$ = createEffect(
    () =>
      this.actions$.pipe(
        ofType(UserActions.fetchUsers),
        exhaustMap(() =>
          from([
            /* Sequence of sequences (observables) of events */
            of(UserActions.loadingUsers()),
            this.fetchUsersAPIrequest()
          ])
        ),
        mergeAll()
      ),
    {
      // dispatch: false
      // resubscribeOnError:true
    }
  );

  saveUserAPIRequest(user: Partial<User>) {
    return this.http.put<User>("http://localhost:3000/users/" + user.id, user);
  }

  fetchUsersAPIrequest() {
    return this.http.get<User[]>("http://localhost:3000/users").pipe(
      map(users => UserActions.loadUsers({ users })),

      catchError(err => {
        return of(UserActions.errorLoadingUsers(err.message));
      })
    );
  }
}
