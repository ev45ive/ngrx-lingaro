import { Component, OnInit } from "@angular/core";
import { Store } from "@ngrx/store";
import { AppState } from "../../reducers/user.reducer";
import { selectUsersList, selectUsersError, selectUsersLoading } from '../../selectors';
import { HttpClient } from "@angular/common/http";
import { User } from "src/app/models/user";
import {
  Observable,
  Subject,
  ConnectableObservable,
  ReplaySubject
} from "rxjs";
import { multicast, refCount, share, shareReplay } from "rxjs/operators";
import { loadUsers, fetchUsers } from "../../actions/user.actions";

@Component({
  selector: "app-browse-users",
  templateUrl: "./browse-users.component.html",
  styleUrls: ["./browse-users.component.scss"]
})
export class BrowseUsersComponent implements OnInit {
  users$ = this.store.select(selectUsersList);
  error$ = this.store.select(selectUsersError);
  loading$ = this.store.select(selectUsersLoading);

  constructor(private http: HttpClient, private store: Store<AppState>) {}

  refresh() {
    // this.http
    //   .get<User[]>("http://localhost:3000/users")
    //   .subscribe(users => this.store.dispatch(loadUsers({ users })));

    this.store.dispatch(fetchUsers());
  }

  ngOnInit() {
    // this.refresh();
  }
}
