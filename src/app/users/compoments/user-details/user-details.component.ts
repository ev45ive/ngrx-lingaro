import { Component, OnInit } from "@angular/core";
import { ActivatedRoute } from "@angular/router";
import { AppState } from "../../reducers/user.reducer";
import { Store, select } from "@ngrx/store";
import { selectUserDetails, selectedUserDetails } from "../../selectors";
import { combineLatest } from "rxjs";
import { map, tap, filter, withLatestFrom } from "rxjs/operators";
import { FormGroup, FormControl } from "@angular/forms";
import { User } from "src/app/models/user";
import { updateUser, saveUser } from "../../actions/user.actions";

@Component({
  selector: "app-user-details",
  templateUrl: "./user-details.component.html",
  styleUrls: ["./user-details.component.scss"]
})
export class UserDetailsComponent implements OnInit {
  // user$ = combineLatest(this.store, this.route.paramMap).pipe(

  // user$ = this.route.paramMap.pipe(
  //   withLatestFrom(this.store),
  //   map(([paramMap, state]) => {
  //     const user_id = paramMap.get("user_id");
  //     return selectUserDetails(state, { user_id }) as User;
  //   }),
  user$ = this.store.select(selectedUserDetails).pipe(
    filter(user => !!user),
    tap(user => {
      this.user = user;
      this.form.setValue({
        name: user.name,
        email: user.email
      });
    })
  );
  user: User;

  form = new FormGroup({
    name: new FormControl(""),
    email: new FormControl("")
  });

  update() {
    const user = {
      id: this.user.id,
      ...this.form.value
    } as User;

    this.store.dispatch(
      saveUser({
        user
      })
    );
  }

  constructor(
    //
    private store: Store<AppState>,
    private route: ActivatedRoute
  ) {
    // route.paramMap.subscribe(paramMap => {
    //   const user_id = paramMap.get("user_id");
    // });
  }

  ngOnInit() {}
}
