import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { NavLayoutComponent } from "../components/nav-layout/nav-layout.component";
import { ProfileComponent } from "./pages/profile/profile.component";
import { BrowseUsersComponent } from "./pages/browse-users/browse-users.component";
import { UserDetailsComponent } from "./compoments/user-details/user-details.component";

const routes: Routes = [
  {
    path: "users",
    component: NavLayoutComponent,
    children: [
      {
        path: "",
        component: BrowseUsersComponent,
        children: [
          {
            path: ":user_id",
            component: UserDetailsComponent
          }
        ]
      },
      {
        path: "profile",
        component: ProfileComponent
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UsersRoutingModule {}
