import { createAction, props } from "@ngrx/store";
import { User } from "../../models/user";

export const fetchUsers = createAction("[User] Fetch Users");

export const loadingUsers = createAction("[User] Loading Users");
export const errorLoadingUsers = createAction(
  "[User] Error Loading Users",
  props<{ error: string }>()
);

export const loadUsers = createAction(
  "[User] Load Users",
  props<{
    users: User[];
  }>()
);

export const createUser = createAction("[User] Create User");

export const updateUser = createAction(
  "[User] Update User",
  props<{ user: Partial<User> }>()
);

export const saveUser = createAction(
  "[User] Save User",
  props<{ user: Partial<User> }>()
);
