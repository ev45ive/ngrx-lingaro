import { Action, createReducer, on } from "@ngrx/store";
import * as UserActions from "../actions/user.actions";
import { User } from "src/app/models/user";
import { AppState } from "../../reducers/index";
import { State as TasksState } from "../../tasks/reducers/task.reducer";

export const userFeatureKey = "user";

export interface AppState {
  [userFeatureKey]: State;
  tasks: TasksState;
}

export interface State {
  users: User[];
  isLoading: boolean;
  error: string;
}

export const initialState: State = {
  users: [],
  isLoading: false,
  error: ""
};

const userReducer = createReducer(
  initialState,

  on(UserActions.loadingUsers, state => ({ ...state, isLoading: true })),
  on(UserActions.loadUsers, (state, { users = [] }) => ({
    ...state,
    users,
    isLoading: false
  })),
  on(UserActions.errorLoadingUsers, (state, action) => ({
    ...state,
    error: action.error
  })),
  on(UserActions.createUser, state => state),
  on(UserActions.updateUser, (state, { user }) => ({
    ...state,
    isLoading:false,
    users: state.users.map(u =>
      u.id == user.id
        ? {
            ...u,
            ...user
          }
        : u
    )
  }))
);

export function reducer(state: State | undefined, action: Action) {
  return userReducer(state, action);
}
