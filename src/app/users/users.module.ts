import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { UsersRoutingModule } from './users-routing.module';
import { ProfileComponent } from './pages/profile/profile.component';
import { StoreModule } from '@ngrx/store';
import * as fromUser from './reducers/user.reducer';
import { EffectsModule } from '@ngrx/effects';
import { UserEffects } from './effects/user.effects';
import { BrowseUsersComponent } from './pages/browse-users/browse-users.component';
import { HttpClientModule } from '@angular/common/http';
import { MatList, MatListModule, MatFormField, MatFormFieldModule, MatInputModule, MatButtonModule, MatIconModule, MatCardModule } from '@angular/material';
import { UserDetailsComponent } from './compoments/user-details/user-details.component';
import { ReactiveFormsModule } from '@angular/forms';


@NgModule({
  declarations: [ProfileComponent, BrowseUsersComponent, UserDetailsComponent],
  imports: [
    CommonModule,
    HttpClientModule,
    UsersRoutingModule,
    StoreModule.forFeature(fromUser.userFeatureKey, fromUser.reducer),
    EffectsModule.forFeature([UserEffects]),
    MatListModule,
    MatFormFieldModule,
    MatInputModule,
    MatButtonModule,
    MatIconModule,
    MatCardModule,
    ReactiveFormsModule,
  ]
})
export class UsersModule {
  // constructor(store:Store){ this.store.dispach(DoSomethingOnStartup)}
 }
