import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { TasksPageComponent } from "./pages/tasks-page/tasks-page.component";
import { NavLayoutComponent } from "../components/nav-layout/nav-layout.component";

const routes: Routes = [
  {
    path: "tasks",
    component: NavLayoutComponent,
    children: [
      {
        path: "",
        component: TasksPageComponent
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TasksRoutingModule {}
