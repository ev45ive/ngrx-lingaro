import {
  Action,
  createReducer,
  on,
  createSelector,
  createFeatureSelector
} from "@ngrx/store";
import * as TaskActions from "../actions/task.actions";
import { Todo } from "src/app/models/todo";
// import { AppState as RootState } from "src/app/reducers";

export const taskFeatureKey = "task";

interface AppState /* extends RootState */ {
  [taskFeatureKey]: State;
}

export const featureSelector = createFeatureSelector<AppState, State>(
  taskFeatureKey
);

export const selectTasks = (state: AppState) =>
  state[taskFeatureKey].items.map(id => state[taskFeatureKey].entities[id]);

export const selectCompleted = createSelector(
  selectTasks,
  tasks => tasks.filter(t => t.completed)
);

export const selectAssignedToUser = createSelector(
  selectTasks,
  (tasks, { userId }) => tasks.filter(t => t.userId == userId)
);

export interface State {
  entities: Record<Todo["id"], Todo>;
  items: Array<Todo["id"]>;
  favourite: Array<Todo["id"]>;
  selectedId: Todo["id"] | null;
}

export const initialState: State = {
  entities: {
    123: {
      id: 123,
      title: "test",
      completed: true,
      userId: 1
    }
  },
  items: [123],
  favourite: [],
  selectedId: null
};

const taskReducer = createReducer(
  initialState,

  on(TaskActions.loadTasks, (state, action) => ({
    ...state,
    items: action.tasks.map(t => t.id),
    entities: action.tasks.reduce<State["entities"]>((entities, task) => {
      return { ...entities, [task.id]: task };
    }, {})
  })),
  on(TaskActions.selectTask, (state, { taskId }) => ({
    ...state,
    selectedId: taskId
  })),
  on(TaskActions.createTask, (state, { task }) => ({
    ...state,
    entities: { ...state.entities, [task.id]: task },
    items: [...state.items, task.id]
  })),
  on(TaskActions.removeTask, (state, action) => ({
    ...state,
    items: state.items.filter(id => id !== action.taskId),
    favourite: state.favourite.filter(id => id !== action.taskId)
  })),
  on(TaskActions.completeTask, (state, { taskId }) => ({
    ...state,
    entities: {
      ...state.entities,
      [taskId]: { ...state.entities[taskId], completed: true }
    }
  })),
  on(TaskActions.markTaskNotComplete, (state, { taskId }) => ({
    ...state,
    entities: {
      ...state.entities,
      [taskId]: { ...state.entities[taskId], completed: false }
    }
  })),
  on(TaskActions.assignTask, (state, { taskId }) => ({
    ...state,
    entities: {
      ...state.entities,
      [taskId]: { ...state.entities[taskId], completed: false }
    }
  }))
);

export function reducer(state: State | undefined, action: Action) {
  return taskReducer(state, action);
}

/* 
// immutable.js / dotProp.js / etc..
const updatedWithPostState = dotProp.set(
  state,
  `posts.byId.${postId}.comments`,
  comments => comments.concat(commentId)
);

const updatedWithCommentsTable = dotProp.set(
  updatedWithPostState,
  `comments.byId.${commentId}`,
  {id : commentId, text : commentText}
);

const updatedWithCommentsList = dotProp.set(
  updatedWithCommentsTable,
  `comments.allIds`,
  allIds => allIds.concat(commentId);
);

*/
