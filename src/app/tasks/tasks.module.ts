import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";

import { TasksRoutingModule } from "./tasks-routing.module";
import { StoreModule } from "@ngrx/store";
import * as fromTask from "./reducers/task.reducer";
import { EffectsModule } from "@ngrx/effects";
import { TaskEffects } from "./effects/task.effects";
import { TasksPageComponent } from "./pages/tasks-page/tasks-page.component";
import {
  MatButtonModule,
  MatListModule,
  MatDividerModule,
  MatInputModule,
  MatCheckboxModule,
  MatIconModule
} from "@angular/material";

@NgModule({
  declarations: [TasksPageComponent],
  imports: [
    CommonModule,
    TasksRoutingModule,
    StoreModule.forFeature(fromTask.taskFeatureKey, fromTask.reducer),
    EffectsModule.forFeature([TaskEffects]),
    [
      MatButtonModule,
      MatListModule,
      MatDividerModule,
      MatInputModule,
      MatCheckboxModule,
      MatIconModule
    ]
  ]
})
export class TasksModule {}
