import { Component, OnInit } from "@angular/core";
import { Store, select } from "@ngrx/store";
import { map, switchMap, withLatestFrom, tap } from "rxjs/operators";
import {
  createTask,
  markTaskNotComplete,
  completeTask
} from "../../actions/task.actions";
import { Todo } from "src/app/models/todo";
import { removeTask } from "../../actions/task.actions";
import {
  State,
  selectTasks,
  selectCompleted,
  selectAssignedToUser
} from "../../reducers/task.reducer";
import { selectUserDetails } from "src/app/users/selectors";
import { User } from "src/app/models/user";
import { Observable, BehaviorSubject, combineLatest } from "rxjs";

@Component({
  selector: "app-tasks-page",
  templateUrl: "./tasks-page.component.html",
  styleUrls: ["./tasks-page.component.scss"]
})
export class TasksPageComponent implements OnInit {
  tasks$ = this.store.select(selectTasks);

  completed$ = this.store.select(selectCompleted);

  assignedToMe$ = this.store.select(state =>
    selectAssignedToUser(state, { userId: 1 })
  );

  selectedTaskUser$: Observable<User>;

  // currentFilter$ = this.assignedToMe$;
  currentFilter$ = this.tasks$;

  constructor(
    private store: Store<{
      task: State;
    }>
  ) {}

  // selectedTaskId$ = new BehaviorSubject<Todo["id"]>(null);
  selectedUserId$ = new BehaviorSubject<Todo["userId"]>(null);

  user$ = combineLatest(this.selectedUserId$, this.store).pipe(
    map(([user_id, state]) => selectUserDetails(state, { user_id })),
  );

  // switchMap(select(state => selectUserDetails(state, { user_id })))
  selected: Todo;
  selectTask(task: Todo) {
    this.selected = task;
    const user_id = task.userId;
    this.selectedUserId$.next(user_id);
  }

  ngOnInit() {}

  createTask(field) {
    const title = field.value;
    const task = {
      id: Date.now(),
      title,
      completed: false,
      userId: 2
    };
    this.store.dispatch(createTask({ task }));
    field.value = "";
  }

  removeTask(id: Todo["id"]) {
    this.store.dispatch(removeTask({ taskId: id }));
  }

  toggleTaskCompleted(task: Todo) {
    const action = task.completed
      ? markTaskNotComplete({ taskId: task.id })
      : completeTask({ taskId: task.id });

    this.store.dispatch(action);
  }
  assignTaskToUser() {}
}
