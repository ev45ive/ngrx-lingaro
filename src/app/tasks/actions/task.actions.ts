import { createAction, props } from "@ngrx/store";
import { Todo } from "src/app/models/todo";

export const loadTasks = createAction(
  "[Task] Load Tasks",
  props<{ tasks: Todo[] }>()
);

export const createTask = createAction(
  "[Task] Create task",
  props<{ task: Todo }>()
);

export const removeTask = createAction(
  "[Task] Remove task",
  props<{ taskId: Todo["id"] }>()
);

export const completeTask = createAction(
  "[Task] Complete Task",
  props<{ taskId: Todo["id"] }>()
);

export const markTaskNotComplete = createAction(
  "[Task] Mark Task Not Complete",
  props<{ taskId: Todo["id"] }>()
);

export const assignTask = createAction(
  "[Task] Assign Task",
  props<{ taskId: Todo["id"]; userId: number }>()
);

export const selectTask = createAction(
  "[Task] Select Task",
  props<{ taskId: Todo["id"] }>()
);
