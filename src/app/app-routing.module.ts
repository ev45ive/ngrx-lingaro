import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { CounterComponent } from "./pages/counter/counter.component";

const routes: Routes = [
  {
    path:'',
    redirectTo:'tasks',
    pathMatch:'full'
  },
  {
    path: "counter",
    component: CounterComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes,{
    // paramsInheritanceStrategy:'always'
  })],
  exports: [RouterModule]
})
export class AppRoutingModule {}
