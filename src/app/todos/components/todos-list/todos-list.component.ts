import {
  Component,
  OnInit,
  Input,
  Output,
  EventEmitter,
  ChangeDetectionStrategy
} from "@angular/core";
import { Todo } from "src/app/models/todo";
import { TodosService } from "../../services/todos.service";

@Component({
  selector: "app-todos-list",
  templateUrl: "./todos-list.component.html",
  styleUrls: ["./todos-list.component.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class TodosListComponent implements OnInit {
  @Input("items")
  todos;

  @Output()
  selectedChange = new EventEmitter<Todo>();

  select(todo: Todo) {
    this.selectedChange.emit(todo);
  }

  toggleCompleted(todo: Todo) {
    this.todoService.toggleCompleted(todo);
  }

  constructor(private todoService: TodosService) {}

  trackFn(item: Todo, index) {
    return item.id;
  }

  ngOnInit() {}
}
