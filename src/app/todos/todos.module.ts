import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";

import { TodosRoutingModule } from "./todos-routing.module";
import { MyTodosComponent } from "./pages/my-todos/my-todos.component";
import {
  MatButtonModule,
  MatListModule,
  MatDividerModule,
  MatInputModule,
  MatIconModule,
  MatCheckboxModule
} from "@angular/material";
import { TodosListComponent } from './components/todos-list/todos-list.component';

@NgModule({
  declarations: [MyTodosComponent, TodosListComponent],
  imports: [
    CommonModule,
    TodosRoutingModule,
    MatButtonModule,
    MatListModule,
    MatDividerModule,
    MatInputModule,
    MatCheckboxModule,
    MatIconModule
  ]
})
export class TodosModule {}
