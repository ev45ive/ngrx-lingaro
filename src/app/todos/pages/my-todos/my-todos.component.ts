import {
  Component,
  OnInit,
  ChangeDetectorRef,
  ChangeDetectionStrategy,
  Inject
} from "@angular/core";
import { Todo } from "src/app/models/todo";
import { TodosService } from "../../services/todos.service";
import { tap } from "rxjs/operators";

@Component({
  selector: "app-my-todos",
  templateUrl: "./my-todos.component.html",
  styleUrls: ["./my-todos.component.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class MyTodosComponent implements OnInit {
  completed$ = this.service.completed;
  todos$ = this.service.todos.pipe(
    tap(todos => {
      /* do something with todos */
    })
  );

  constructor(private service: TodosService) {
    // this.service.todos.subscribe() // .unsubscribe()
  }

  ngOnDestroy() {}

  completedOnly() {
    this.todos$ = this.service.completed;
  }

  ngOnInit() {}

  createTodoFrom(formField) {
    this.service.createNewTodo(formField.value);
    formField.value = "";
  }
}
