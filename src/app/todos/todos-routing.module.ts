import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { MyTodosComponent } from "./pages/my-todos/my-todos.component";
import { NavLayoutComponent } from "../components/nav-layout/nav-layout.component";

const routes: Routes = [
  {
    path: "todos",
    component: NavLayoutComponent,
    children: [
      {
        path: "",
        component: MyTodosComponent
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TodosRoutingModule {}
