import { Injectable, EventEmitter } from "@angular/core";
import { Todo } from "src/app/models/todo";
import { BehaviorSubject } from "rxjs";

import { map } from "rxjs/operators";

@Injectable({
  providedIn: "root"
})
export class TodosService {
  toggleCompleted(todo: Todo) {
    todo = {
      ...todo,
      completed: !todo.completed
    };

    this._todos.next(
      this._todos.getValue().map(
        // Replace old todo with new version
        t => (t.id == todo.id ? todo : t)
      )
    );
  }

  private _todos = new BehaviorSubject<Todo[]>([
    {
      id: 123,
      title: "Get milk",
      completed: true,
      userId: 1
    },
    {
      id: 234,
      title: "Learn Angular",
      completed: false,
      userId: 1
    },
    {
      id: 345,
      title: "Profit!",
      completed: false,
      userId: 1
    }
  ]);

  todos = this._todos.asObservable();

  completed = this.todos.pipe(map(todos => todos.filter(t => t.completed)));

  createNewTodo(title) {
    const todo = {
      id: Date.now(),
      title: title,
      completed: false,
      userId: 1
    };
    this._todos.next([...this._todos.getValue(), todo]);
  }

  constructor() {
    (window as any).subject = this._todos
  }
}
