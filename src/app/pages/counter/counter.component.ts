import { Component, OnInit } from "@angular/core";
import { Store } from "@ngrx/store";
import { AppState } from "src/app/reducers";
import { map } from "rxjs/operators";
import { Increment, Decrement, ResetCounters } from '../../reducers/counter.actions';

@Component({
  selector: "app-counter",
  templateUrl: "./counter.component.html",
  styleUrls: ["./counter.component.scss"]
})
export class CounterComponent implements OnInit {
  counter$ = this.store.pipe(map(state => state.counter.value));

  constructor(private store: Store<AppState>) {}

  increment() {
    this.store.dispatch(new Increment(1));
  }

  decrement() {
    this.store.dispatch(new Decrement(1));
  }

  reset() {
    this.store.dispatch(new ResetCounters());
  }

  ngOnInit() {}
}
