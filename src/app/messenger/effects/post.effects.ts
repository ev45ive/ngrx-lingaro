import { Injectable } from "@angular/core";
import { Actions, createEffect, ofType } from "@ngrx/effects";
import { catchError, map, concatMap, exhaustMap } from "rxjs/operators";
import { EMPTY, of } from "rxjs";

import * as PostActions from "../actions/post.actions";
import { MessengerApiService } from "../messenger-api.service";

@Injectable()
export class PostEffects {
  loadPosts$ = createEffect(() =>
    this.actions$.pipe(
      ofType(PostActions.loadPosts),
      exhaustMap(() =>
        this.api.fetchPosts().pipe(
          map(posts => PostActions.loadPostsSuccess({ posts })),
          catchError(error => of(PostActions.loadPostsFailure({ error })))
        )
      )
    )
  );

  deletePost$ = createEffect(() =>
    this.actions$.pipe(
      ofType(PostActions.deletePost),
      exhaustMap(({ id }) =>
        this.api.deletePost(id).pipe(
          map(data => PostActions.deletePostSuccess({ id })),
          catchError(error => of(PostActions.deletePostFailure({ error })))
        )
      )
    )
  );

  constructor(private api: MessengerApiService, private actions$: Actions) {}
}
