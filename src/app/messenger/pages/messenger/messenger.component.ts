import { Component, OnInit } from "@angular/core";
import { Store } from "@ngrx/store";
import { selectAllPosts } from "../../reducers/post.reducer";
import { loadPosts, deletePost } from "../../actions/post.actions";
import { Post } from "src/app/models/post";
import { PostsService } from "../../posts.service";
import { EntityActionFactory, EntityOp } from "@ngrx/data";

@Component({
  selector: "app-messenger",
  templateUrl: "./messenger.component.html",
  styleUrls: ["./messenger.component.scss"]
})
export class MessengerComponent implements OnInit {
  // posts$ = this.store.select(selectAllPosts);
  // posts$ = this.service.getAll();
  posts$ = this.service.entities$;

  constructor(
    private entityActionFactory: EntityActionFactory,
    private service: PostsService,
    private store: Store<any>
  ) {}

  deletePost({ id }: Post) {
    // const action = this.entityActionFactory.create(
    //   "Post",
    //   EntityOp.REMOVE_ONE,
    //   id,
    //   {}
    // );
    this.service.delete({ id } as Post, {
      // isOptimistic:true
    });
    // this.store.dispatch(action);
    // this.store.dispatch(deletePost({ id }));
  }

  ngOnInit() {
    this.service.getAll();
    // this.store.dispatch(loadPosts());
  }
}
