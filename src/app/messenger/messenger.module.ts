import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";

import { MessengerRoutingModule } from "./messenger-routing.module";
import { StoreModule } from "@ngrx/store";
import * as fromPost from "./reducers/post.reducer";
import { EffectsModule } from "@ngrx/effects";
import { PostEffects } from "./effects/post.effects";
import * as fromComment from "./reducers/comment.reducer";
import { CommentEffects } from "./effects/comment.effects";
import { MessengerComponent } from "./pages/messenger/messenger.component";
import { EntityDataModule } from "@ngrx/data";

@NgModule({
  declarations: [MessengerComponent],
  imports: [
    CommonModule,
    MessengerRoutingModule,
    StoreModule.forFeature(fromPost.postFeatureKey, fromPost.reducer),
    EffectsModule.forFeature([PostEffects, CommentEffects]),
    StoreModule.forFeature(fromComment.commentFeatureKey, fromComment.reducer),
  ]
})
export class MessengerModule {}
