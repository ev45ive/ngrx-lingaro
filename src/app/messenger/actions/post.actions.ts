import { createAction, props } from "@ngrx/store";
import { Post } from "src/app/models/post";

export const loadPosts = createAction("[Post] Load Posts");

export const addPost = createAction("[Post] Add Post", props<{ post: Post }>());

export const removePost = createAction(
  "[Post] Remove Post",
  props<{ id: Post["id"] }>()
);

export const loadPostsSuccess = createAction(
  "[Post] Load Posts Success",
  props<{ posts: Post[] }>()
);

export const loadPostsFailure = createAction(
  "[Post] Load Posts Failure",
  props<{ error: any }>()
);

export const deletePost = createAction(
  "[Post] Delete Post",
  props<{ id: Post["id"] }>()
);
export const deletePostSuccess = createAction(
  "[Post] Delete Post Sucess",
  props<{ id: Post["id"] }>()
);
export const deletePostFailure = createAction(
  "[Post] Delete Post Failure",
  props<{ error: any }>()
);
