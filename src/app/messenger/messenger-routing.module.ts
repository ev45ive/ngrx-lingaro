import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { MessengerComponent } from "./pages/messenger/messenger.component";
import { NavLayoutComponent } from "../components/nav-layout/nav-layout.component";

const routes: Routes = [
  {
    path: "messenger",
    component: NavLayoutComponent,
    children: [
      {
        path: "",
        component: MessengerComponent
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MessengerRoutingModule {}
