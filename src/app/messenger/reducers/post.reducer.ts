import {
  Action,
  createReducer,
  on,
  createFeatureSelector,
  createSelector
} from "@ngrx/store";
import * as PostActions from "../actions/post.actions";
import { EntityState, createEntityAdapter } from "@ngrx/entity";
import { Post } from "src/app/models/post";
import { removePost } from "../actions/post.actions";

export const postFeatureKey = "post";

export interface State extends EntityState<Post> {
  selectedId: Post["id"] | null;
  isLoading: boolean;
}

const postAdapter = createEntityAdapter<Post>({
  // selectId,
  // sortComparer
});

export const initialState: State = postAdapter.getInitialState({
  selectedId: null,
  isLoading: false
});

const postReducer = createReducer<State>(
  initialState,

  on(PostActions.addPost, (state, { post }) =>
    postAdapter.upsertOne(post, state)
  ),
  on(PostActions.deletePostSuccess, (state, { id }) =>
    postAdapter.removeOne(id, state)
  ),
  on(PostActions.loadPosts, state => ({ ...state, isLoading: true })),
  on(PostActions.loadPostsSuccess, (state, action) =>
    postAdapter.addAll(action.posts, {
      ...state,
      isLoading: false
    })
  ),
  on(PostActions.loadPostsFailure, (state, action) => ({
    ...state,
    isLoading: false
  }))
);

export function reducer(state: State | undefined, action: Action) {
  return postReducer(state, action);
}

const {
  selectAll,
  selectEntities,
  selectIds,
  selectTotal
} = postAdapter.getSelectors();

export const featureSelector = createFeatureSelector(postFeatureKey);

export const selectAllPosts = createSelector(
  featureSelector,
  selectAll
);
