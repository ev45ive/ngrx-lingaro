import { TestBed } from '@angular/core/testing';

import { MessengerApiService } from './messenger-api.service';

describe('MessengerApiService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: MessengerApiService = TestBed.get(MessengerApiService);
    expect(service).toBeTruthy();
  });
});
