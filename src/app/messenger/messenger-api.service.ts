import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { Post } from "../models/post";

@Injectable({
  providedIn: "root"
})
export class MessengerApiService {
  deletePost(id: number) {
    return this.http.delete(this.api_url + "posts/" + id);
  }

  private api_url = "http://localhost:3000/";

  constructor(private http: HttpClient) {}

  fetchPosts() {
    return this.http.get<Post[]>(`${this.api_url}posts/`);
  }
}
