inc = ({type:'INC', payload:1});
dec = ({type:'DEC', payload:1});
addTodo = title => ({type:"ADD_TODO", payload: title});

[inc,inc,addTodo('placki'), inc,dec].reduce( (state, action)=>{
    switch(action.type){
        case 'INC': return { ...state, counter: state.counter + action.payload };
        case 'DEC': return { ...state, counter: state.counter - action.payload };
        case 'ADD_TODO': return { ...state, todos:[...state.todos, action.payload]};
        default: return state;
    }
},{
	counter:0, placki:123,todos:[]
})

inc = ({type:'INC', payload:1});
dec = ({type:'DEC', payload:1});
addTodo = title => ({type:"ADD_TODO", payload: title});

function counterReducer(state=0, action){
 switch(action.type){
        case 'INC': return state.counter + action.payload ;
        case 'DEC': return state.counter - action.payload ;
        default: return state;
}

function reducer(state, action){
   	return {
		...state,
		counter: counterReducer(state.counter,action),
        todos: todosReducer(state.todos,action),
        nested:{
            placki: plackiReducer(state.nexted.placki,action)
        }
    }
}

mapReducersHelper({
	counter: counterReducer,
	todos: todosReducer
})
